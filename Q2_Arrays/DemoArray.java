/**
 * This create a demoArray. 
 * @author (kevin jackman) 
 * @version (2.0 2/2/16)
 */

import java.util.ArrayList;
import java.util.Iterator;

public class DemoArray {

    private ArrayList<Integer> numBers;
    private int[] intArray;
    private int size = 10;
    private int[] eVenNumBers;
    private int range;
    private int counter=0;
    private int sum=0;

    /**
     * Constructs a demo Array object
     * within the constructor an int Array is constructed
     * @param size
     * 
     * within the constructor an ArrayList of integers is also constructed
     * 
     */
    public DemoArray() 
    {
        intArray  = new int[size];

    }

    /**
     * Use a for loop to initialize each element in the array with 100 + i.
     * Use a similar loop to output the array.
     */
    public void demoArray() 
    {

        for(int i=0; i< intArray.length; i++)
        {
            intArray[i]= i+100;
        }

        for(int j=0; j< intArray.length; j++)
        {       
            System.out.println("Element at index "+ j +" is" + " " + intArray[j]);
        }

    }

    /**
     * Use a while loop to initialize each element in the array with 200 + 2*i.
     * Use a similar loop to output the array.
     */
    public void demoArray2() {

        int i=0;

        while (i<intArray.length)
        {
            intArray[i]= 200+ i*2;
            i++;
        }

        int counter=0;

        while (counter< intArray.length)
        {
            System.out.println("Element at index "+ counter +" is" + " " + intArray[counter]);
            counter++;
        }

    }

    /**
     * Use a do-while loop to initialize each element in the array with 300 + 3*i.
     * Use a similar loop to output the array.
     */
    public void demoArray3() {
        int i=0;
        int counter=0;

        do
        { 
            intArray[i]=300+3*i;
            i++;
        }
        while(i<intArray.length);

        do

        { 
            System.out.println("Element at index "+ counter +" is" + " " + intArray[counter]);
            counter++;
        }
        while(counter<intArray.length);
    }

    /**
     * Use an iterator to initialize each element in the array with 400 + 4*i.
     * Use a similar loop to output the array.
     */
    public void demoArray4() 
    {    
        numBers = new ArrayList<Integer>();

        for(int i=0; i<size; i++)
        {
            numBers.add(400+(4*i));
        }

        Iterator<Integer> it = numBers.iterator(); 

        int j=0;

        while(it.hasNext())
        {   
            //System.out.println(it.next());
            System.out.println("Element at index "+ j +" is" + " " + it.next());
            j++;
        }

    }

    /**
     * Create an array of even numbers in range [low, hi]
     * Precondition: low and hi must be even. 
     * Enforce precondition: print message if not met and return immediately.
     * Calculate and print the sum of these numbers.
     * @param low The minimum range value inclusive
     * @param hi The maximum range value inclusive
     */

    public void sumEven(int low, int hi)
    {

        if((low%2==0)&&(hi%2==0))
        {

            for( int i=low; i<=hi; i++)
            {    
                if (i%2==0)
                {
                    range++;

                }

            }

            eVenNumBers = new int[range]; 

            for(int i =low; i <=hi; i++)
            {    
                if (i%2==0)
                {
                    eVenNumBers[counter]=i;
                    counter++;
                }

            }

            for(int s:eVenNumBers)
            {
                sum+=s;
            }

            System.out.println("the sum of the even numbers between  " + low + "  and  "+ hi + "  is  " + sum);
        }
        else
        {
            System.out.println("Please make both numbers Even");
        }

    }

}
