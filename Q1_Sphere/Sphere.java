
/**
 * Write a description of class Sphere here. 
 * @author (kevin jackman) 
 * @version (2.0 2/2/16)
 */
public class Sphere
{
    double radius;
    private static int numberSpheres=0;
    boolean isValid;

    /**
     * Constructs a default Sphere object    
     */
    public Sphere()
    {
        setState(10.);

    } 

    /**
     * Constructs a new Sphere object defined by user-supplied parameter
     * @param radius   
     */

    public Sphere(double radius)
    { 
        setState(radius); 

    }

    /**
     * provides a method to set the default State of Sphere.
     * tests weather the param is valid
     * @param radius
     */
    private void setState(double radius)
    {

        if (isValid(radius))
        {
            this.radius=radius; 
            isValid=true;
            numberSpheres++;

        }

        else
        {
            System.out.println("You have attemped to create a sphere with a negative radius\nOnly positive radii permitted");   
        }

    }

    /**
     * Calculates the surface area of the Sphere.
     * @return The total surface area of Sphere.
     */
    public double surfaceArea()
    {
        return 4 * Math.PI * radius * radius;
    }

    /**
     * Calculates the volume of a Sphere object.
     * @return The volume of a Sphere.
     */
    public double volume()
    {
        return  ( 4.0 / 3.0 ) * Math.PI * Math.pow(radius,3);

    }

    /**
     * Calculates the circumference of a Great Circle.
     * @return The cicumference of a Sphere.
     */
    public double greatCircle()
    {    
        return Math.PI*(radius*2);
    }

    /**
     * allows you to change the circle size.
     * @param radius.
     */
    public void changeSize(double radius)
    {
        setState(radius);  
    }

    /**
     * returns the amount of circle objects created.
     */
    public int getNumberSpheres()
    {
        return numberSpheres;
    }

    /**
     * tests a value to see weather it is true or false > < 0.
     * @param value.
     */
    private boolean isValid(double value)
    {  
        return (value>0)?  true:false;

    }
} 

