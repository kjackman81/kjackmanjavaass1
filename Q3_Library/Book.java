
/**
 * @file    Book.java
 * @brief   This class defines the basic description of a book and provides some characteristic methods.
 * @author (kevin jackman) 
 * @version (2.0 2/2/16)
 */

public class Book
{
    private String   title;
    private String   author;
    private String   isbn;
    private int      numberPages;
    private boolean  borrowed;
    private int      numberBorrowings;

    /**
     * Constructs a new Book object defined by user-supplied parameters.
     * @param title         Name of book
     * @param auther        Person who wrote book
     * @param numberOfpages Amount of pages in book
     * @param isbn          ID on book
     */

    public Book(String title, String author, int numberPages, String isbn)
    {   
        this.title= title;
        this.author=author;
        this.numberPages=numberPages;
        this.isbn=isbn;
    }

    /**
     * The method borrow facilitates borrowing the book
     * varible is set to true
     * number of books is incremented
     */
    public void borrow() // borrow a book
    {
        borrowed=true;
        numberBorrowings++;
    }

    /**
     * boolean method returns is book on loan
     * returns the status of the borrowed varible
     */
    public boolean isBorrowed() // is book on loan?#
    {
        return borrowed;    
    }

    /**
     * sets the value of the varible to false when returned
     */
    public void returns()       // return book 
    {
        borrowed=false;
    }

    /**
     * prints out statement weather book is available or not 
     */

    public String loanStatus()  // print a message
    {
        if(borrowed==true)
        {
            return ("Book titled " + title + ": Not available: preasently on loan");
        }
        else
        {
            return( "Book titled "+ title + ": is available to borrow");

        }
    }

    /**
     * a print statement describing each book object
     */

    public void printDetails()  // print details re book
    {
        System.out.println("Title         : " +   title);  
        System.out.println("Author        : " +   author); 
        System.out.println("Pages         : " +   numberPages); 
        System.out.println("ISBN          : " +   isbn); 
        System.out.println("Borrowed      : " +   numberBorrowings +" times"); 
        System.out.println("Availability  : " +   loanStatus()); 
        System.out.println(); 

    }   

    /**
     * returns the title of the book
     */
    public String getTitle()      // return book title
    {
        return title;
    }

}
