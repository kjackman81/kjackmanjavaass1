import java.util.ArrayList;
import java.util.Iterator;

public class Library
{
    ArrayList<Book> books = new ArrayList<Book>();

    /**
     * adds a book to the  ArrayList
     * @param book
     */
    public void add(Book book)
    {
        books.add(book);

    }

    /**
     * returns the amount of books in the ArrayList
     */
    public int  numberBooks()
    {
        return books.size();
    }

    /**
     * calls the book method loanStatus to print the books status
     * @param book
     */
    public String   loanStatus(Book book)
    {
        return  book.loanStatus();
    }

    /**
     * looking for a particular book in the ArrayList(if it contains it)
     * @param book
     */
    public String  hasBook(Book book)
    {
        if (books.contains(book))
        {  
            return (book.loanStatus());

        }

        else 
        {   
            return(book.loanStatus());
        }
       

    }

    /**
     * clearing a particular book from the ArrayList
     * @param book
     */
    public boolean removeBook(Book book)
    {
        if (books.contains(book))
        {    
            books.remove(book);
            return true;
        }

        else
        {   
            System.out.println("cant find book");
            return false;
        }
    }

    /**
     * clearing all the book from the ArrayList(empty)
     */
    public void removeAllBooks()
    {
        books.clear();  
    }

    /**
     * a print statement describing all the book objects in the ArrayList
     */
    public void printDetailsAll()
    {
        for(Book b:books)
        {
            b.printDetails();
        }
    }

}
