
/**
 * The ClockDisplay class implements a digital clock display for a
 * European-style 24 hour clock. The clock shows hours and minutes. The 
 * range of the clock is 00:00 (midnight) to 23:59 (one minute before 
 * midnight).
 * 
 * The clock display receives "ticks" (via the timeTick method) every minute
 * and reacts by incrementing the display. This is done in the usual clock
 * fashion: the hour increments when the minutes roll over to zero.
 * 
 * @author kjackman
 * @version (2.1 9/2/16)
 */
public class ClockDisplay
{   
    private NumberDisplay seconds;
    private NumberDisplay minutes;
    private NumberDisplay hours;
    private NumberDisplay days;
    private String displayTime; // simulates the actual display
    private int sec = 60;
    private int min = 60;
    private int hR = 24;
    private int dY = 365;

    /**
     * Constructor for ClockDisplay objects. This constructor 
     * creates a new clock set at 000:00:00:00.
     */
    public ClockDisplay()
    {
        seconds = new NumberDisplay(sec);
        minutes = new NumberDisplay(min);
        hours = new NumberDisplay(hR);
        days   =  new NumberDisplay(dY);
        updateDisplay();
    }

    /**
     * Constructor for ClockDisplay objects. This constructor
     * creates a new clock set at the time specified by the 
     * parameters.
     * @param second
     * @param minute
     * @param hour
     * @param day
     */
    public ClockDisplay(int second, int minute, int hour, int day)
    {
        seconds = new NumberDisplay(sec);
        minutes = new NumberDisplay(min);
        hours = new NumberDisplay(hR);
        days   =  new NumberDisplay(dY);
        setTime(second, minute, hour, day);
    }

    /**
     * Update the internal string that represents the display.
     */
    private void updateDisplay()
    {
        this.displayTime = days.display()+":"+hours.display()+":"+ minutes.display()+":"+ seconds.display();
    }

    /**
     * Set the time of the display to the specified day, hour, minute, second
     * @param second
     * @param minute
     * @param hour
     * @param day
     */
    public void setTime(int second,int minute, int hour, int day)
    {   
        seconds.setValue(second);
        minutes.setValue(minute);
        hours.setValue(hour);
        days.setValue(day);
        updateDisplay();
    }

    /**
     * enables the user to set the seconds to a inputted value
     * @param value
     */
    public void secondsSet(int value)
    {
        seconds.setValue(value);
    }

    /**
     * enables the user to set the minutes to a inputted value
     * @param value
     */
    public void minutesSet(int value)
    {
        minutes.setValue(value);   
    }

    /**
     * enables the user to set the hours to a inputted value
     * @param value
     */
    public void hoursSet(int value)
    {
        hours.setValue(value);
    }

    /**
     * enables the user to set the days to a inputted value
     * @param value
     */

    public void daysSet(int value)
    {
        days.setValue(value);
    }

    /**
     * This is where the increment method is called to start the ticking of the clock
     * uses the modulus in the getValue method as a parameter  
     */
    public void timeTick()
    {
        seconds.incrementValue();

        if(seconds.getValue()==0)
        {
            minutes.incrementValue();
        }

        if(minutes.getValue()==0 && seconds.getValue()==0)
        {
            hours.incrementValue();
        }

        if(hours.getValue()==0 && minutes.getValue()==0 && seconds.getValue()==0)
        {
            days.incrementValue();
        }

        updateDisplay();

    }

    /**
     * Return the current time of this display in the format DD:HH:MM:SS.
     */
    public String getTime()
    {
        return displayTime;
    }

}
