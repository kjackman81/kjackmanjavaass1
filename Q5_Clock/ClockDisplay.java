
/**
 * The ClockDisplay class implements a digital clock display for a
 * European-style 24 hour clock. The clock shows hours and minutes. The 
 * range of the clock is 00:00 (midnight) to 23:59 (one minute before 
 * midnight).
 * 
 * The clock display receives "ticks" (via the timeTick method) every minute
 * and reacts by incrementing the display. This is done in the usual clock
 * fashion: the hour increments when the minutes roll over to zero.
 * 
 * @author kjackman
 * @version (2.1 9/2/16)
 */

public class ClockDisplay
{
    private NumberDisplay seconds;
    private NumberDisplay hours;
    private NumberDisplay minutes;
    private String displayTime;    // simulates the actual display

    /**
     * Constructor for ClockDisplay objects. This constructor 
     * creates a new clock set at 00:00:00.
     */
    public ClockDisplay()
    {
        seconds = new NumberDisplay(60);
        minutes = new NumberDisplay(60);
        hours    = new NumberDisplay(24);
        updateDisplay();
    }

    /**
     * Constructor for ClockDisplay objects. This constructor
     * creates a new clock set at the time specified by the 
     * parameters.
     * @param second
     * @param minute
     * @param hour
     */
    public ClockDisplay(int second, int minute, int hour)
    {   
        seconds = new NumberDisplay(60);
        minutes = new NumberDisplay(60);
        hours = new NumberDisplay(24);
        setTime(hour, minute, second);
    }

    /**
     * This method is used to specify when the incrementing
     * process takes place using both incrementValue and getValue methods
     */
    public void timeTick()
    {
        seconds.incrementValue();

        if(seconds.getValue()==0)
        {
            minutes.incrementValue();
        }

        if(minutes.getValue() == 0 && seconds.getValue()==0) 
        {  // it just rolled over!
            hours.incrementValue();
        }

        updateDisplay();
    }

    /**
     * Set the time of the display to the specified hour and
     * minute second.
     * @param hour
     * @param minute
     * @param second
     * 
     */
    public void setTime(int hour, int minute,int second)
    {
        seconds.setValue(second);
        minutes.setValue(minute);
        hours.setValue(hour);
        updateDisplay();
    }

    /**
     * enables the user to set the seconds to a inputted value
     * @param value
     */
    public void secondsSet(int value)
    {
        seconds.setValue(value);
    }

    /**
     * enables the user to set the minutes to a inputted value
     * @param value
     */
    public void minutesSet(int value)
    {
        minutes.setValue(value);   
    }

    /**
     * enables the user to set the hours to a inputted value
     * @param value
     */
    public void hoursSet(int value)
    {
        hours.setValue(value);
    }

    /**
     * Return the current time of this display in the format HH:MM.
     */
    public String getTime()
    {
        return displayTime;
    }

    /**
     * Update the internal string that represents the display.
     */
    private void updateDisplay()
    {
        displayTime = hours.display() + ":" + 
        minutes.display()+ ":" +seconds.display();
    }
}
